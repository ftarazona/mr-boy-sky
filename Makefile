ASSETS_DIR=assets
SRC_DIR=src
ALT_SRC_DIR=aux_src
BUILD_DIR=build

APP=main
SRC=$(SRC_DIR)/$(APP).s
OBJ=$(BUILD_DIR)/$(APP).o
LINK=$(BUILD_DIR)/$(APP).lnk
ROM=$(BUILD_DIR)/$(APP).gb

MUSICFILE=$(ASSETS_DIR)/music.bin
MUSICSRC=$(ASSETS_DIR)/music.mc
MUSIC_CONVERTER=music_converter
MUSIC_CONVERTER_EXE=$(BUILD_DIR)/$(MUSIC_CONVERTER)
MUSIC_CONVERTER_SRC=$(ALT_SRC_DIR)/$(MUSIC_CONVERTER).c

MAPFILE=$(ASSETS_DIR)/map.bin
MAPSRC=$(ASSETS_DIR)/map.mp
MAP_CONVERTER=map_converter
MAP_CONVERTER_EXE=$(BUILD_DIR)/$(MAP_CONVERTER)
MAP_CONVERTER_SRC=$(ALT_SRC_DIR)/$(MAP_CONVERTER).c

TILEMAP=$(ASSETS_DIR)/tiles.bin

$(ROM): $(OBJ) $(LINK)
	wlalink -d -r -v -s $(LINK) $@

$(OBJ): $(SRC) $(MUSICFILE) $(MAPFILE) $(TILEMAP)
	wla-gb -I $(ASSETS_DIR) -o $@ $<

$(LINK):
	echo "[objects]" > $@
	echo "$(OBJ)" >> $@

$(MUSICFILE): $(MUSICSRC) $(MUSIC_CONVERTER_EXE)
	./$(MUSIC_CONVERTER_EXE) $< $@

$(MUSIC_CONVERTER_EXE): $(MUSIC_CONVERTER_SRC)
	gcc -o $@ $<

$(MAPFILE): $(MAPSRC) $(MAP_CONVERTER_EXE)
	./$(MAP_CONVERTER_EXE) $< $@

$(MAP_CONVERTER_EXE): $(MAP_CONVERTER_SRC)
	gcc -o $@ $<

clean:
	rm $(ROM)
	rm $(LINK)
	rm $(OBJ)

clean-all:
	rm -rf $(BUILD_DIR)
