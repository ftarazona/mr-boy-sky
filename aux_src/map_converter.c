#include <stdlib.h>
#include <stdio.h>

char map[32 * 32];
char map_out[32 * 32 + 1];

int main(int argc, char * argv[])   {
    if(argc < 3)    {
        printf("Usage: ./map_converter <map input> <map_output>\n");
        exit(EXIT_FAILURE);
    }

    FILE * f = fopen(argv[1], "r");
    if(f == NULL)   {
        printf("Could not open file %s in R mode\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    for(unsigned l = 0; l < 32; l++)    {
        fread(map + (l * 32), sizeof(char), 32, f);
        getc(f);
    }
    fclose(f);

    map_out[0] = 0;
    for(unsigned i = 0; i < 32 * 32; i++)   {
        map_out[i + 1] = map[32 * 32 - i - 1] - '0';
        printf("%d", map_out[i + 1]);
    }

    FILE * f_out = fopen(argv[2], "wb");
    if(f == NULL)   {
        printf("Could not open file %s in WB mode\n", argv[2]);
        exit(EXIT_FAILURE);
    }
    
    fwrite(map_out, sizeof(char), 32 * 32 + 1, f_out);
    fclose(f_out);
}
