#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define N_SCALES 3
#define NOTES_PER_SCALES 12

float frequencies[NOTES_PER_SCALES * N_SCALES] = {
    220.00, 233.08, 246.94, 261.63, 277.18, 293.66, 311.13, 329.63, 349.23, 369.99, 392.00, 415.30,
    440.00, 466.16, 493.88, 523.25, 554.37, 587.33, 622.25, 659.25, 698.46, 739.99, 783.99, 830.61,
    880.00, 932.33, 987.77, 1046.50, 1108.73, 1174.66, 1244.51, 1318.51, 1396.91, 1479.98, 1567.98, 1661.22 };

uint16_t frequency_to_integer(float freq)    {
    float wl = 2048. - (131072. / freq);
    printf("Note hexa: %04x\n", (uint16_t)wl);
    return (uint16_t)wl;
}

uint16_t str_to_note(const char * str)   {
    printf("Converting '%s' to note\n", str);
    int note = 0;
    int i = 0;
    switch(str[i])    {
        case 'A': note = 0; break;
        case 'B': note = 2; break;
        case 'C': note = 3; break;
        case 'D': note = 5; break;
        case 'E': note = 7; break;
        case 'F': note = 8; break;
        case 'G': note = 10; break;
        default: return 0xFFFF;
    }
    i++;
    switch(str[i])  {
        case '#': note++; i++; break;
        case 'b': note--; i++; break;
        case 0: return frequency_to_integer(frequencies[note]);
        default: break;
    }
    switch(str[i])  {
        case '3': return frequency_to_integer(frequencies[note]);
        case '4': return frequency_to_integer(frequencies[12 + note]);
        case '5': return frequency_to_integer(frequencies[24 + note]);
        case 0: return frequency_to_integer(frequencies[note]);
        default: return 0xFFFF;
    }
}

uint8_t invert_unit_len(uint8_t unit_len) {
    switch(unit_len) {
        case 1: return 64;
        case 2: return 32;
        case 4: return 16;
        case 8: return 8;
        case 16: return 4;
        case 32: return 2;
        default: return 0;
    }
}

uint8_t invert_len(uint8_t len) {
    uint8_t ret = 0;
    for(unsigned i = 0; i < 8; i++) {
        ret += invert_unit_len(len & (1 << i));
    }
    return ret;
}

int main(int argc, char *argv[])    {

    if(argc != 3)   {
        printf("Usage : ./music_converter <input music> <output music>\n");
        exit(EXIT_FAILURE);
    }

    FILE * fin = fopen(argv[1], "r");
    if(fin == NULL) {
        printf("Could not open %s as input\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    int n = 0;
    while(!feof(fin))    {
        if (fgetc(fin) == '\n') {
            n++;
        }
    }
    rewind(fin);

    uint16_t * notes = (uint16_t *) malloc(n * sizeof(uint16_t));
    uint8_t * lens = (uint8_t *) malloc(n * sizeof(uint8_t));

    for(unsigned i = 0; i < n; i++) {
        char node_str[3];
        fscanf(fin, "%s %d", node_str, &(lens[i]));
        lens[i] = invert_len(lens[i]);
        notes[i] = str_to_note(node_str);
        if(lens[i] == 0xFF || notes[i] == 0xFFFF)   {
            printf("Invalid note: %s with len %d\n", node_str, lens[i]);
            exit(EXIT_FAILURE);
        }
    }
    fclose(fin);

    FILE * fout = fopen(argv[2], "wb");
    if(fout == NULL)    {
        printf("Could not open %s for writing\n", argv[2]);
        exit(EXIT_FAILURE);
    }

    uint16_t len = n * 3;
    printf("Final len : %04x\n", len);
    fputc(len & 0x00FF, fout);
    fputc((len & 0xFF00) >> 8, fout);
    for(unsigned i = 0; i < n; i++) {
        fputc(notes[n - i - 1] & 0x00FF, fout);
        fputc((notes[n - i - 1] & 0xFF00) >> 8, fout);
        fputc(lens[n - i - 1], fout);
    }
    fclose(fout);

    free(notes);
    free(lens);

    return 0;
}
