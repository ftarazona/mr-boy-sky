; This is a simple scroller
; The player can move to the left or to the right
; on a fixed background
;
; The background is 256pixels long.

.ROMDMG
.NAME "SCROLLER"
.CARTRIDGETYPE 0
.RAMSIZE 0
.COMPUTECBCHECKSUM
.COMPUTEGBCOMPLEMENTCHECKSUM
.LICENSEECODENEW "00"
.EMPTYFILL $00

.MEMORYMAP
    SLOTSIZE $4000
    DEFAULTSLOT 0
    SLOT 0 $0000
    SLOT 1 $4000
.ENDME

.ROMBANKSIZE $4000
.ROMBANKS 2

.BANK 0 SLOT 0

; -- posX indicates the absolute position of X --
; Note that this position is lower bounded by 0.
; Note that it is upper bounded by 256 - 8 = 252 (sprite width)
.ENUM $C000
    posX    DB
    posY    DB
    vitY    DB
    jumping DB
    update_speed    DB
.ENDE

; When the vblank interruption is raised, the processor
; jumps to the first instruction of vblank label.
; Once it returns, it returns from this handler and reenables interrupts
.ORG $0040
    call    vblank
    reti

.ORG $0100
    nop             ; a NOP is inserted according to Nintendo recommendations
    jp      start

.ORG $0104
.DB $CE,$ED,$66,$66,$CC,$0D,$00,$0B,$03,$73,$00,$83,$00,$0C
.DB $00,$0D,$00,$08,$11,$1F,$88,$89,$00,$0E,$DC,$CC,$6E,$E6
.DB $DD,$DD,$D9,$99,$BB,$BB,$67,$63,$6E,$0E,$EC,$CC,$DD,$DC
.DB $99,$9F,$BB,$B9,$33,$3E


; Starting point of the program

;
; Init sequence
;
.ORG $0150
start:
    di                  ; Disable interrupts
    ld      sp, $FFF4   ; Init Stack Pointer

    xor     a
    ldh     ($26), a    ; Disable audio

waitvbl:            ; -- Waiting for vblank, i.e. $FF44 >= 144 --
    ldh     a, ($44)
    cp      144
    jr      c, waitvbl
    xor     a
    ldh     ($40), a    ; Disable screen


;
; Graphics data loading
;
; Our graphics data consists in 3 tiles.
; Tiles 0 and 1 are in the background.
; Tile 2 is to draw the player.
;

; -- Load tiles to VRAM (0x8000) --
    ld      b, 3 * 8 * 2
    ld      de, tiles
    ld      hl, $8000
load_tiles:
    ld      a, (de)
    ldi     (hl), a
    inc     de
    dec     b
    jr      nz, load_tiles

; -- Draw Background in VRAM (0x9800) --
; The background consists in alternate vertical lines :
;   010101010101
;   010101010101
;   010101010101
;
; In order to create such a pattern we unroll the loop in order to write
; 01 for each iteration. There are 32 * 32 tiles in total, so 32 * 16 iterations
    ld      de, 32 * 32
    ld      hl, $9800
draw_bg:
    ld      a, d
    ld      c, e
    add     $0A
    ld      b, a
    ld      a, (bc)
    ldi     (hl), a
    dec     de
    ld      a, e
    or      d
    jr      nz, draw_bg

; -- Player initialization --
;
; We initialize posX, the player sprit, then draw the first screen
    xor     a
    ld      (jumping), a
    ld      (update_speed), a

    ld      a, 122      ; Initialize posX
    ld      (posX), a

    ld      a, 4 * 8        ; Initialize posY
    ld      (posY), a

    ld      hl, $FE02   ; Initialize sprite tile index
    ld      a, 2
    ld      (hl), a

    inc     l           ; Ensure the attributes of the sprite are reset
    xor     a
    ld      (hl), a

    call    draw        ; Draw the first screen

; -- Palettes initializations (0xFF47, 0xFF48, 0xFF49) --
    ld      a, %11100100
    ldh     ($47), a
    ldh     ($48), a
    ldh     ($49), a

; -- LCD control initialization --
    ld      a, %10010011
    ldh     ($40), a
    ld      a, %00010000
    ldh     ($41), a

; -- Enable VBLANK interrupt --
    ld      a, %00000001
    ldh     ($FF), a
    ei

; Once the initialization sequence is complete,
; we loop infinitely.
; The loop will be periodically interrupted by 
; vblank interrupts, triggering the logic to process
loop:
    jr      loop


; -- VBLANK interrupt logic --
;
; We process periodically the modification of the player's position.
; We also display the new position.
vblank:

; -- Save af and hl registers --
    push    af
    push    hl

; -- Get joypad input (left/right/no input) --
dir_input:
    ld      a, %00100000    ; Configure input register into LR/UD mode
    ldh     ($00), a
    ldh     a, ($00)        ; Get input of joypad
    ld      b, a

; -- Update posX (absolute X position) --
    ld      a, (posX)
    bit     $0, b           ; has player pressed RIGHT ?
    jr      nz, nor         ; if NOT, jump to nor (NoRight)
    inc     a
    cp      248             ; Check overflow
    jr      nc, overflow
    jr      no_overflow
overflow:
    ld      a, 248
    ld      (posX), a
    call    lowbeep
    jr      update_pos_y
no_overflow:
    ld      (posX), a
    jr      update_pos_y    ; Jump to next action
nor:
    bit     $1, b           ; has player pressed LEFT ?
    jr      nz, nol         ; if NOT, jump to nol (NoLeft)
    cp      0               ; Check underflow
    jr      z, underflow
    dec     a
    jr      no_underflow
underflow:
    call    lowbeep
    jr      update_pos_y
no_underflow:
    ld      (posX), a
    jr      update_pos_y       ; Jump to the next action
nol:
    ld      (posX), a       ; No update
    jr      update_pos_y

; -- Update posY (absolute Y position --

update_pos_y:
; -- Get button A input for jumping --
    ld      a, %00010000
    ldh     ($00), a
    ldh     a, ($00)
    ld      b, a

    bit     $0, b
    jr      nz, apply_gravity_preamble

    ld      a, (jumping)
    cp      0
    jr      nz, apply_gravity_preamble

    inc     a
    ld      (jumping), a

    ld      a, (vitY)
    ld      a, 4
    ld      (vitY), a

; Gravity applies as an acceleration of -1.
; The maximum speed for falling is -7
apply_gravity_preamble:
    ld      a, (update_speed)
    cp      2
    jr      z, apply_gravity
    inc     a
    ld      (update_speed), a
    jr      update_position
apply_gravity:
    xor     a
    ld      (update_speed), a

    ld      a, (vitY)
    sub     1
    ;cp      -7
    ;jr      nc, vmax_not_reached
    ;ld      a, -7
vmax_not_reached:
    ld      (vitY), a
    ld      b, a

update_position:
    ld      a, (vitY)
    ld      b, a
    ld      a, (posY)
    ld      c, a
    add     a, b
    ld      (posY), a
    ld      d, a

; -- Check collision with floor cell (platform) --
check_collision:
    and     a, %11111000    ; Cell reached
    ld      d, a

    ld      a, c            ; Old cell
    and     a, %11111000
    ld      c, a

    ld      a, d
    cp      c
    jr      c, falling    ; Cell did not change: no collision with floor happened
    jr      call_draw

; We searched for the cell reached to check whether it is floor (platform = 1) or not
; In the BG map, this is in (BG map base address) + cell_y * 32 + cell_x + 1
; Note : register c should not be modified as posY will be set to it in case there was a collision
falling:
    ld      h, 0
    ld      l, d
    sla     l
    jr      nc, sla2
    inc     h
    inc     h
sla2:
    sla     l
    jr      nc, sla3
    inc     h
sla3:

    ld      a, (posX)
    srl     a
    srl     a
    srl     a
    ld      b, a
    ld      a, 32
    sub     b
    add     l
    ld      l, a
    ld      a, h
    add     $0A
    ld      h, a

    ld      a, (hl)
    bit     $0, a
    jr      nz, floor

; Possible second cell to check
    ld      a, (posX)
    cp      248             ; The cell is rightmost, no second chance
    jr      nc, call_draw

    and     %00000111       ; Checking whether we are at the second half of a cell
    cp      2
    jr      c, call_draw    ; We were not in the rightmost half of our cell, abort

    ld      h, 0
    ld      l, d
    sla     l
    jr      nc, sla2_1
    inc     h
    inc     h
sla2_1:
    sla     l
    jr      nc, sla3_1
    inc     h
sla3_1:


    ld      a, (posX)
    srl     a
    srl     a
    srl     a
    add     1
    ld      b, a
    ld      a, 32
    sub     b
    add     l
    ld      l, a
    ld      a, h
    add     $0A
    ld      h, a

    ld      a, (hl)
    bit     $0, a
    jr      z, call_draw

floor:
    xor     a
    ld      (jumping), a
    ld      (vitY), a
    ld      a, c
    ld      (posY), a

call_draw:
    call    draw

; -- Restore HL and AF registers and return --
    pop     hl
    pop     af
    ret


; -- General drawing --
; Given absolute position, draws the player sprite and set the BG scroll
; 
; The difficulty stems from the fact our map is bounded (0 to 256)
; Then there are three cases, depending on whether the player is in the
; leftmost part or rightmost part of the map.
; 
; The leftmost (resp rightmost) part is defined as the leftmost (resp rightmost) 
;  part of the viewport when the latter is at its left (resp right) bound.
; We remind the viewport has a 160-pixel width.
;                               144-pixel height
; Therefore its maximum value (viewing the rightmost part of BG) is 256 - 160 = 96
;                              viewing the bottom part of BG is 256 - 144 = 112
;
; The player is in the middle of the viewport when it is centered. Careful : we must
;  take its own width into account !
; Then it is in the middle when it is at X = 160 / 2 - 8 / 2 = 76 pixels from the X
;  left position of the viewport.
;                                        Y = 144 / 2 - 8 / 2 = 68 pixels from the Y up pos
; 
; This leads to defined our zones as such :
;  leftmost zone => posX < 0 + 76 = 76
;  upmost zone => posY < 0 + 68 = 68
;  rightmost zone => posX > 96 + 76 = 172
;  bottom zone => posY > 112 + 68 = 180
;  middle zone => anywhere in-between
;
; Once we determined the zone we are in, we determine how the player's position
;  in the viewport and the background scrolling both vary !
; Leftmost zone => BG at 0, POS = posX
; Upmost zone => BG at 0, POS = posY
; Rightmost zone => BG at 96, POS = posX - 172 + 76
; Bottom zone => BG at 80, POS = posY - 180 + 68
; Middle zone => BG at posX - 76, POS = 76
;                BG at posY - 68, POS = 68
draw:
draw_x:
    ld      a, (posX)
    cp      76
    jr      nc, not_in_left_zone    ; jumps if posX >= 76

    ld      hl, $FE01       ; Update player sprite
    add     8
    ld      (hl), a
    
    xor     a               ; Update BG scrolling
    ldh     ($43), a
    
    jr      draw_y

not_in_left_zone:

    cp      172
    jr      c, not_in_right_zone     ; jumps if posX < 172

    ld      hl, $FE01       ; Update player sprite
    sub     96              ; 172 - 76
    add     8
    ld      (hl), a

    ld      a, 96           ; Update BG scrolling
    ldh     ($43), a

    jr      draw_y

not_in_right_zone:

    sub     76              ; Update BG scrolling
    ldh     ($43), a

    ld      a, 76           ; Update player sprite
    add     8
    ld      hl, $FE01
    ld      (hl), a

draw_y:
    ld      a, (posY)
    ld      b, a
    ld      a, 255
    sub     b
    cp      68
    jr      nc, not_in_up_zone    ; jumps if posY >= 68

    ld      hl, $FE00       ; Update player sprite
    add     9
    ld      (hl), a
    
    xor     a               ; Update BG scrolling
    ldh     ($42), a
    
    jr      draw_end

not_in_up_zone:

    cp      180
    jr      c, not_in_bottom_zone     ; jumps if posY < 148

    ld      hl, $FE00       ; Update player sprite
    sub     112             ; 180 - 68
    add     9
    ld      (hl), a

    ld      a, 112           ; Update BG scrolling
    ldh     ($42), a

    jr      draw_end

not_in_bottom_zone:

    sub     68              ; Update BG scrolling
    ldh     ($42), a

    ld      a, 68           ; Update player sprite
    add     9
    ld      hl, $FE00
    ld      (hl), a

draw_end:
    ret

lowbeep:
    call    set_audio 

    ld      a, %00000000    ; 8 lower bits of wavelength
    ldh     ($13), a
    ld      a, %11000111    ; Enable CH1, 3 higher bits of wavelengths
    ldh     ($14), a

    ret

set_audio:
    ld      a, %10000000    ; Turn on the APU
    ldh     ($26), a

    ld      a, %01110111    ; CH1,2,3 LR outputs enabled
    ldh     ($24), a
    ld      a, %00010001    ; Volume of LR outputs to 1
    ldh     ($25), a

    ld      a, %10011000    ; CH1 : length timer: 56, wave duty: 50%
    ldh     ($11), a        ; this means 8 cycles at 256Hz before turned off (~300ms)
    ld      a, %11110000    ; CH1 : initial volume of envelop : max
    ldh     ($12), a

    ret

.ORG $0800
tiles:
.INCBIN "tiles.bin"

.ORG $0A00
map:
.INCBIN "map.bin"
