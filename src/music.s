; This is a simple music player
;
; We draw a fix screen in the background
; and play a music described in file music.bin

.ROMDMG
.NAME "SCROLLER"
.CARTRIDGETYPE 0
.RAMSIZE 0
.COMPUTECBCHECKSUM
.COMPUTEGBCOMPLEMENTCHECKSUM
.LICENSEECODENEW "00"
.EMPTYFILL $00

.MEMORYMAP
    SLOTSIZE $4000
    DEFAULTSLOT 0
    SLOT 0 $0000
    SLOT 1 $4000
.ENDME

.ROMBANKSIZE $4000
.ROMBANKS 2

.BANK 0 SLOT 0

.ORG $0040
    nop
    reti

.ENUM $C000
    timer_count     DB
    music_pointer   DW
.ENDE

; Our timer interrupt handler
; We must implement a software timer to reach a second, because the timer clock will not
; go below 4KHz, which the timer can not interrupt below 8Hz
.ORG $0050
    ld      a, (timer_count)
    dec     a
    jr      nz, skip_timer

    ld      de, music_pointer   ; We load the address of music_pointer var
    ld      a, (de)             ; We load music_pointer var into hl
    inc     de
    ld      h, a
    ld      a, (de)
    ld      l, a

    ld      b, (hl)     ; b contains the higher bits of wavelength
    inc     hl
    ld      c, (hl)     ; c contains the lower byte of wavelength
    inc     hl

    ld      a, (hl)  ; Reload timer
    ld      (timer_count), a
    inc     hl

    ld      de, music_pointer   ; de contains address of music_pointer var
    ld      a, h                ; loading hl into var
    ld      (de), a
    inc     de
    ld      a, l
    ld      (de), a

    call    low_beep
    reti
skip_timer:
    ld      (timer_count), a
    reti

.ORG $0100
    nop
    jp      start

.ORG $0104
.DB $CE,$ED,$66,$66,$CC,$0D,$00,$0B,$03,$73,$00,$83,$00,$0C
.DB $00,$0D,$00,$08,$11,$1F,$88,$89,$00,$0E,$DC,$CC,$6E,$E6
.DB $DD,$DD,$D9,$99,$BB,$BB,$67,$63,$6E,$0E,$EC,$CC,$DD,$DC
.DB $99,$9F,$BB,$B9,$33,$3E

; Starting point of the program

;
; Init sequence
;
.ORG $0150
start:
    di                  ; Disable interrupts
    ld      sp, $FFF4   ; Init stack pointer

    xor     a           ; Disable APU
    ldh     ($26), a

waitvbl:        ; -- Waiting for vblank before writing to VRAM --
    ldh     a, ($44)
    cp      144
    jr      c, waitvbl
    xor     a
    ldh     ($40), a

    ld      b, 8 * 2
    ld      de, tiles
    ld      hl, $8000
load_tiles:
    ld      a, (de)
    ldi     (hl), a
    inc     de
    dec     b
    jr      nz, load_tiles

    ld      de, 32 * 32
    ld      hl, $9800
draw_bg:
    xor     a
    ldi     (hl), a
    dec     de
    ld      a, e
    or      d
    jr      nz, draw_bg

    ld      a, %11100100
    ldh     ($47), a
    ldh     ($48), a
    ldh     ($49), a

    ld      a, %10010011
    ldh     ($40), a
    ld      a, %00010000
    ldh     ($41), a


; Here comes the intersting part : we must initialize what it will require to play sound
; For the moment we will simply activate a beep each second.
;
; For this we configure the timer interrupt to trigger an interrupt each second.
; The handler will then call low_beep
    xor     a               ; Set the Timer Modulo to max
    ldh     ($06), a        ; TIMA will overflow at rate 4096 / 512 = 8Hz

    ld      a, %00000100    ; Sets the timer clock to 4096Hz and enable timer.
    ldh     ($07), a        ; TIMA begins to count
    
    ld      hl, music_pointer   ; Load address of music_pointer
    ld      de, music           ; Load base address of our bin music
    ld      (hl), d             ; Write base address into music_pointer
    inc     hl
    ld      (hl), e

    ld      de, music_pointer   ; Reload address of music_pointer var
    ld      a, (de)             ; Loading music_pointer into de
    ld      h, a
    inc     de                  ; de contains the base address of our bin music
    ld      a, (de)
    ld      l, a
    ld      a, (hl)             ; Load the first timer counter
    ld      (timer_count), a
    inc     hl                  ; increase music_pointer
    ld      de, music_pointer
    ld      a, h
    ld      (de), a
    inc     de
    ld      a, l
    ld      (de), a
    ld      a, %00000101    ; enables vblank and timer
    ldh     ($FF), a
    ei

loop:
    jr      loop


low_beep:
    call    set_audio 

;    ld      a, %00000000    ; 8 lower bits of wavelength
    ld      a, c
    ldh     ($13), a
 ;   ld      a, %11000111    ; Enable CH1, 3 higher bits of wavelengths
    ld      a, b
    or      %11000000
    ldh     ($14), a

    ret

set_audio:
    ld      a, %10000000    ; Turn on the APU
    ldh     ($26), a

    ld      a, %01110111    ; CH1,2,3 LR outputs enabled
    ldh     ($24), a
    ld      a, %00010001    ; Volume of LR outputs to 1
    ldh     ($25), a

    ld      a, %10010000    ; CH1 : length timer: 56, wave duty: 50%
    ldh     ($11), a        ; this means 8 cycles at 256Hz before turned off (~300ms)
    ld      a, %11110000    ; CH1 : initial volume of envelop : max
    ldh     ($12), a

    ret

.ORG $0800
tiles:
.INCBIN "tiles.bin"

.ORG $0900
music:
.DB $08,$07,$06,$10,$07,$21,$08,$07,$3A,$10,$07,$44,$08,$07,$59,$10,$07,$6B,$08,$07,$7B,$10,$07,$83
