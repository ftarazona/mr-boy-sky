# Mr Boy Sky

This is a Game Boy basic platform game. The name is simply because the music playing in the background will be Mr. Blue Sky. The music playing in the background will be Mr. Blue Sky because I love this song. For any further question on this very bad pun, please do not ask me. Cheh. 

## About me and this project

I am Florian Tarazona, a 3rd-year student at Télécom Paris. This project was inspired by The Game Boy of Isaac (https://github.com/PainsPerdus/gboi-rocket/tree/testing/src) made for a 1st-year project by a team of 6 students.
It aims at getting familiar with assembly programming and way of thinking, using a simple architecture (8-bit Z80), in a funny manner.

## Acknowledgements

I wish to thank the maintainers of Pandocs (https://gbdev.io/pandocs) which made a fairly exhaustive documentation for the gameboy.
